# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application repository is for phone database where user can create,update,delete,get the phone using API's 

### How do I get set up? ###
This application is on Spring Boot. So just run the application as Spring boot and get yuor application running.

Create database phone in mysql.
For configuring mysql database please change application.properties in resource folder

### Design ###

The application is using MAVEN for dependencies. And using Spring boot.

PhoneController.java  -> PhoneServiceImpl.java -> PhoneFacadeImpl.java -> PhoneDAO.java

PhoneControllerTests.java - For Unit testing

PhoneController.java - All CRUD URI have their handler methods in this class. It consumes the json of TO object

PhoneServiceImpl.java - This class has all the validations like if the phone exists, user species mandatory fields or not. If any error found user throws error.

MessageTO<T>.java - This class is used a transport object which has all the details like Object/ErroMessage, HTTPStatus Code 

PhoneFacadeImpl.java -  This class calls DAO layer. This class in future can call file operations when object is saved. Facade pattern is used here.

PhoneDAO.java - This interface implements CRUDRepository and handles all CRUD opertaions

SINGLE_TABLE_STRATERGY JPA
Phone.java - this is entity abstract class which will be saved into table
AndroidPhone.java,IOSPhone.java,WindowsPhone.java - Concrete implementation of Phone.java

    
PhoneType.java - Enum class with phonetype- android , IOS, windows
StoreType.java - Enum class with storetype - playstore,winstore,istore


### API Details ###
GET URI : localhost:8080/phone/1

POST URI : localhost:8080/phone
body: {"phoneType":"android","storeType":"Playstore","model":"Nexus8","price":100}

PUT URI : localhost:8080/phone
body: {"phoneType":"android","storeType":"Playstore","model":"Nexus8","price":100,"phoneId":1}

DELETE URI: localhost:8080/phone/1


## Validations ##

GET - check if is passed is present or Throw 404 Not Found exception , if everything ok 200 OK
POST - check if type is passed. Check if mandatory fields like model and price passed. If not 
       400 bad request. If everything is good and object is created. Pass the created object        with ID and 201 created status code

PUT - check only model and price can be changed, id should be passed. If not 400 Bd request or    200 OK

Delete -  check if phone is present if not 404 Not found if present delete and send 200 OK


