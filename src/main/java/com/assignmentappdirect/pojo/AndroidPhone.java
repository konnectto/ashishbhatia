package com.assignmentappdirect.pojo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="android")

public class AndroidPhone extends Phone{

	
	public AndroidPhone()
	{
		this.phoneType = PhoneType.Android;
		this.storeType=StoreType.Playstore;
	}
	

	@Override
	public void outgoingCall() {
		System.out.println("Android outgoing call");		
	}

	@Override
	public void incomingCall() {
		System.out.println("Android incoming call");
		
	}

	@Override
	public void outgoingSMS() {
		System.out.println("Android outgoing sms");
		
	}

	@Override
	public void incomingSMS() {
		System.out.println("Android incoming sms");
		
	}



}
