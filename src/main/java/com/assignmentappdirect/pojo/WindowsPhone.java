package com.assignmentappdirect.pojo;

public class WindowsPhone extends Phone{

	
	public WindowsPhone()
	{
		this.phoneType = PhoneType.Windows;
		this.storeType=StoreType.WinStore;
	}
	

	@Override
	public void outgoingCall() {
		System.out.println("Windows outgoing call");		
	}

	@Override
	public void incomingCall() {
		System.out.println("Windows incoming call");
		
	}

	@Override
	public void outgoingSMS() {
		System.out.println("Windows outgoing sms");
		
	}

	@Override
	public void incomingSMS() {
		System.out.println("Windows incoming sms");
		
	}


}
