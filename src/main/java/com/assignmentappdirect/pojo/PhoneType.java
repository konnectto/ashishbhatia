package com.assignmentappdirect.pojo;

public enum PhoneType {
    
	Android("Android"),
	IOS("IOS"),
	Windows("Windows");
	
	
	private String type;

	PhoneType(String type) {
        this.type = type;
    }
	
    public String getType() {
        return this.type;
    }



}
