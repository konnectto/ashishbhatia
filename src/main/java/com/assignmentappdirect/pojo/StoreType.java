package com.assignmentappdirect.pojo;

public enum StoreType {
    
	Playstore("Playstore"),
	Istore("Istore"),
	WinStore("WinStore");
	
	
	private String type;

	StoreType(String type) {
        this.type = type;
    }
	
    public String getType() {
        return this.type;
    }



}
