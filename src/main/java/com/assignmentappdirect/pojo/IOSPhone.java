package com.assignmentappdirect.pojo;

public class IOSPhone extends Phone{

	
	
	public IOSPhone(){
		this.phoneType = PhoneType.IOS;
		this.storeType =StoreType.Istore;
	}
	
	
	

	@Override
	public void outgoingCall() {
		System.out.println("IOSPhone outgoing call");		
	}

	@Override
	public void incomingCall() {
		System.out.println("IOSPhone incoming call");
		
	}

	@Override
	public void outgoingSMS() {
		System.out.println("IOSPhone outgoing sms");
		
	}

	@Override
	public void incomingSMS() {
		System.out.println("IOSPhone incoming sms");
		
	}


	
	
}
