package com.assignmentappdirect.pojo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance
@DiscriminatorColumn(name="type")

public abstract class Phone {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	
	@NotNull
	protected String model;
	
	@NotNull
	protected BigDecimal price;
	
	@NotNull
	@Column(name="createdDate")

	protected Date createdDtm;
	@Column(name="phone_type")
	@Enumerated(EnumType.STRING)
	protected PhoneType phoneType;
	@Column(name="store_type")
	@Enumerated(EnumType.STRING)
	protected StoreType storeType;

	
	public void setPhoneType(PhoneType phoneType) {
		this.phoneType = phoneType;
	}
	public void setStoreType(StoreType storeType) {
		this.storeType = storeType;
	}
	public Date getCreatedDate() {
		return createdDtm;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDtm = createdDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getModel() {
		return model;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public StoreType getStoreType(){
		return storeType;
	}
	public PhoneType getPhoneType(){
		return phoneType;
	}
	public abstract void outgoingCall();
	public abstract void incomingCall();
	public abstract void outgoingSMS();
	public abstract void incomingSMS();
}
