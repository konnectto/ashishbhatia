package com.assignmentappdirect.service;

import com.assignmentappdirect.to.MessageTO;
import com.assignmentappdirect.to.PhoneTO;

public interface PhoneService {
	
	public MessageTO<?> getPhone(Long phoneId);
	public MessageTO<?> createPhone(PhoneTO phoneTO);
	public MessageTO<?> deletePhone(Long phoneId);
	public MessageTO<?> updatePhone(PhoneTO phoneTO);

}
