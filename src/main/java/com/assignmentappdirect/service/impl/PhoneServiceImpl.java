package com.assignmentappdirect.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.assignmentappdirect.facade.PhoneFacade;
import com.assignmentappdirect.pojo.Phone;
import com.assignmentappdirect.service.PhoneService;
import com.assignmentappdirect.to.MessageTO;
import com.assignmentappdirect.to.PhoneTO;

@Service("phoneService")
public class PhoneServiceImpl implements PhoneService{

	@Autowired
	PhoneFacade phoneFacade;
	


	/*
	 * (non-Javadoc)
	 * @see com.assignmentappdirect.service.PhoneService#getPhone(java.lang.Long)
	 */
	@Override
	public MessageTO<?> getPhone(Long phoneId) {
		Phone phone = phoneFacade.getPhone(phoneId);
		//check if phone is available or not. 
		//If available set the messageTO to valid and attach the phone object with response code OK
		if(phone != null){
			PhoneTO phoneTO = new PhoneTO(phone);
			MessageTO<PhoneTO> messagePhone = new MessageTO<>();
			messagePhone.setMessage(phoneTO);
			messagePhone.setValid(true);
			messagePhone.setResponseStatus(HttpStatus.OK);
			return messagePhone;
			 
		}
		//If not available set the messageTO to not valid and attach the error message with response code Not Found
		else{
			MessageTO<String> messagePhone = new MessageTO<>();
			messagePhone.setMessage("Not found"); //I18n will use resource bundle
			messagePhone.setValid(false);
			messagePhone.setResponseStatus(HttpStatus.NOT_FOUND);
			return messagePhone;
		}
		
	}

	
	
	
	/*
	 * (Javadoc)
	 * @see com.assignmentappdirect.service.PhoneService#createPhone(com.assignmentappdirect.to.PhoneTO)
	 */
	@Override
	public MessageTO<?> createPhone(PhoneTO phoneTO) {
		StringBuilder builder = null;
		if(phoneTO.getPrice() == null){
			
			if(builder ==null)
				builder = new StringBuilder();
			builder.append("Price missing in request");
				
		}
		if(phoneTO.getModel() == null){
			if(builder ==null)
				builder = new StringBuilder();
			builder.append("Model missing in request");
		}
		
		if(builder != null){
			MessageTO<String> messageTO = new MessageTO<>();
			messageTO.setValid(false);
			messageTO.setMessage(builder.toString());
			
			messageTO.setResponseStatus(HttpStatus.BAD_REQUEST);
			return messageTO;

		}
		Phone phone = phoneFacade.createPhone(PhoneTO.getPhonePoJo(phoneTO));
		
		MessageTO<PhoneTO> messageTO = new MessageTO<>();
		messageTO.setValid(true);
		messageTO.setMessage(new PhoneTO(phone));
		messageTO.setResponseStatus(HttpStatus.CREATED);
		return messageTO;
	
	}




	@Override
	public MessageTO<?> deletePhone(Long phoneId) {

		MessageTO<?> messageTO = getPhone(phoneId);
		MessageTO<String>  messageRes = new MessageTO<String>();

		if(messageTO.isValid()){
			phoneFacade.deletePhone(phoneId);
			messageRes.setValid(true);
			messageRes.setResponseStatus(HttpStatus.OK);
			messageRes.setMessage("Delete successfully");
			
			return messageRes;
		}
		else{
			messageRes.setValid(false);
			messageRes.setMessage("Phone not present. Please check the id passed in request");
			
			messageTO.setResponseStatus(HttpStatus.NOT_FOUND);
			return messageTO;

		}
	
	}
		
	
	public MessageTO<?> updatePhone(PhoneTO phoneTO){
		MessageTO<?> messageTO = getPhone(phoneTO.getPhoneId());

		if(messageTO.isValid()){
			if(!((PhoneTO)(messageTO.getMessage())).getPhoneType().equalsIgnoreCase(phoneTO.getPhoneType())){
				MessageTO<String>  messageRes = new MessageTO<String>();
				messageRes.setValid(false);
				messageRes.setMessage("Cannot change phone type");
				
				messageRes.setResponseStatus(HttpStatus.BAD_REQUEST);
				return messageRes;
			}
			Phone phone = phoneFacade.updatePhone(PhoneTO.getPhonePoJo(phoneTO));
			
			MessageTO<PhoneTO> messagePh = new MessageTO<>();
			messagePh.setValid(true);
			messagePh.setMessage(new PhoneTO(phone));
			messagePh.setResponseStatus(HttpStatus.OK);
			return messagePh;
		}
		else{
			MessageTO<String>  messageRes = new MessageTO<String>();
			messageRes.setValid(false);
			messageRes.setMessage("Phone not present. Please check the id passed in request");
			
			messageRes.setResponseStatus(HttpStatus.NOT_FOUND);
			return messageRes;

		}
	}
	
	
}
