package com.assignmentappdirect.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.assignmentappdirect.dao.impl.PhoneDao;
import com.assignmentappdirect.facade.PhoneFacade;
import com.assignmentappdirect.pojo.Phone;


@Component("phoneFacade")
public class PhoneFacadeImpl implements PhoneFacade {
	
	@Autowired
	PhoneDao phoneDao;
	
	public Phone getPhone(Long phoneId){
		return phoneDao.findOne(phoneId);
	}
	
	public Phone createPhone(Phone phone){
		return phoneDao.save(phone);
		//return getPhone(phone.getId());
		
	}
	
	
	public void deletePhone(Long phoneId){
		phoneDao.delete(phoneId);

	}

	public Phone updatePhone(Phone phone){
		Phone origPhone = phoneDao.findOne(phone.getId());
		origPhone.setModel(phone.getModel());
		origPhone.setPrice(phone.getPrice());
		return phoneDao.save(origPhone);
		//return getPhone(phone.getId());


	}



}
