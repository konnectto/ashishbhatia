package com.assignmentappdirect.facade;

import com.assignmentappdirect.pojo.Phone;

public interface PhoneFacade {
	public Phone getPhone(Long phoneId);
	public Phone createPhone(Phone phone);
	public void deletePhone(Long phoneId);
	public Phone updatePhone(Phone phone);

}
