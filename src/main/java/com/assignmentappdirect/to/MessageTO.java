package com.assignmentappdirect.to;

import org.springframework.http.HttpStatus;

public class MessageTO<T> {
private boolean isValid;
private T  message;
private HttpStatus responseStatus;
public boolean isValid() {
	return isValid;
}
public void setValid(boolean isValid) {
	this.isValid = isValid;
}
public T getMessage() {
	return message;
}
public void setMessage(T message) {
	this.message = message;
}
public HttpStatus getResponseStatus() {
	return responseStatus;
}
public void setResponseStatus(HttpStatus responseStatus) {
	this.responseStatus = responseStatus;
}



	
}
