package com.assignmentappdirect.to;

import java.math.BigDecimal;
import java.util.Date;

import com.assignmentappdirect.pojo.AndroidPhone;
import com.assignmentappdirect.pojo.IOSPhone;
import com.assignmentappdirect.pojo.Phone;
import com.assignmentappdirect.pojo.PhoneType;
import com.assignmentappdirect.pojo.WindowsPhone;

public class PhoneTO {
	private String phoneType;
	private String storeType;
	private String model;
	private BigDecimal price;
	private long phoneId;
	private Date createdDate;
	
	public PhoneTO(){
		
	}
	public PhoneTO(Phone phone){
		this.phoneType = phone.getPhoneType().getType();
		this.model = phone.getModel();
		this.price = phone.getPrice();
		this.phoneId = phone.getId();
		this.createdDate = phone.getCreatedDate();
		this.storeType=phone.getStoreType().getType();
		this.phoneId=phone.getId();
	}
	
	
	
	
	
	public String getStoreType() {
		return storeType;
	}





	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}





	public Date getCreatedDate() {
		return createdDate;
	}





	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}





	public long getPhoneId() {
		return phoneId;
	}
	public void setPhoneId(long phoneId) {
		this.phoneId = phoneId;
	}
	public String getPhoneType() {
		return phoneType;
	}
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	

	public static Phone getPhonePoJo(PhoneTO phoneTO){
		
		Phone phone = null;
		
		if(phoneTO.getPhoneType().equalsIgnoreCase(PhoneType.Android.getType())){
			phone = new AndroidPhone();
		}
		else if(phoneTO.getPhoneType().equalsIgnoreCase(PhoneType.Windows.getType())){
			phone = new WindowsPhone();
		}
		else if(phoneTO.getPhoneType().equalsIgnoreCase(PhoneType.IOS.getType())){
			phone = new IOSPhone();
		}

		phone.setModel(phoneTO.getModel());
		phone.setPrice(phoneTO.getPrice());
		phone.setCreatedDate(new Date());
		phone.setId(phoneTO.getPhoneId());
		return phone;
		
		
		
	}
	

}
