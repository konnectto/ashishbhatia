package com.assignmentappdirect.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assignmentappdirect.pojo.Phone;
@Repository ("phoneDao") 
public interface PhoneDao extends CrudRepository<Phone, Long>  	  {
	
	

}
