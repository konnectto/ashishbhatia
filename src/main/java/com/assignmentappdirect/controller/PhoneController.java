package com.assignmentappdirect.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignmentappdirect.service.PhoneService;
import com.assignmentappdirect.to.MessageTO;
import com.assignmentappdirect.to.PhoneTO;



@RestController
public class PhoneController {

	@Autowired
	PhoneService phoneService;
	
	/**
	 * 
	 * @param phoneId
	 * @return
	 */
	@RequestMapping(value = "/phone/{phoneId}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<?> handleGetPhone(
	        @PathVariable("phoneId") Long phoneId) {

		MessageTO<?> messageTO = null;
		
		//calling service to get the phone from id
		messageTO = phoneService.getPhone(phoneId);

		return new ResponseEntity<>(messageTO.getMessage(), messageTO.getResponseStatus());


	}

	
	
	/**
	 * 
	 * @param phoneId
	 * @return
	 */
	@RequestMapping(value = "/phone", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<?> handleCreatePhone(@RequestBody
	        PhoneTO phoneTO) {

		MessageTO<?> messageTO = null;
		
		//calling service to get the phone from id
		messageTO = phoneService.createPhone(phoneTO);

		if(messageTO.isValid()){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Last-Modified", ((PhoneTO)messageTO.getMessage()).getCreatedDate().toString());
		}
		return new ResponseEntity<>(messageTO.getMessage(), messageTO.getResponseStatus());


	}


	
	/**
	 * 
	 * @param phoneId
	 * @return
	 */
	@RequestMapping(value = "/phone/{phoneId}", method = RequestMethod.DELETE,produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<?> handleDeletePhone(
			 @PathVariable("phoneId") Long phoneId) {

		MessageTO<?> messageTO = null;
		
		//calling service to get the phone from id
		messageTO = phoneService.deletePhone(phoneId);


		return new ResponseEntity<>(messageTO.getMessage(), messageTO.getResponseStatus());


	}

	/**
	 * 
	 * @param phoneId
	 * @return
	 */
	@RequestMapping(value = "/phone", method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<?> handleUpdatePhone(
			 @RequestBody PhoneTO phoneTO) {

		MessageTO<?> messageTO = null;
		
		//calling service to get the phone from id
		messageTO = phoneService.updatePhone(phoneTO);


		return new ResponseEntity<>(messageTO.getMessage(), messageTO.getResponseStatus());


	}
	
	
}
